from distutils.core import setup, Extension

# Third-party modules - we depend on numpy for everything
import numpy

# Obtain the numpy include directory.  This logic works across numpy versions.
try:
    numpy_include = numpy.get_include()
except AttributeError:
    numpy_include = numpy.get_numpy_include()

pvt_module = Extension('_pvt_table',
                       sources=['PVTTable.cpp', 'PVTTable.i'],
                       include_dirs=[numpy_include],
                       extra_compile_args = ['-fPIC', '-stdlib=libc++', '-std=c++14'],
                       swig_opts=['-c++', '-builtin', '-modern', '-py3'],
                       extra_link_args=['-stdlib=libc++']
)

setup(name='pvt_table', ext_modules=[pvt_module], py_modules=["pvt_table"])
