# scripting-poc

This project describes a way to extend C++ classes with Python scripts. To run this project the following requirements have to be satisfied:

- A working Python environement with `numpy`, `scipy` and `matplotlib` needs to be present in the system. The project right now detects the default Python interpreter, i.e. the one that launches when you type `python` in the command line;
- The `pybind11` library from [here](https://github.com/pybind/pybind11). Just create the `pybind11` folder and copy the repo contents into it;
- A `filesystem` library, either from `std` or [here](https://github.com/gulrak/filesystem). In the latter case, create the `ghc` folder and copy header files from the library into it.