#include "PVTTable.h"

#include <fstream>
#include <iostream>
#include <sstream>

using std::stod;
namespace {
std::vector<std::string> split(const std::string& s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter)) {
        if (!token.empty()) tokens.push_back(token);
    }
    return tokens;
}

// trim from start (in place)
inline void ltrim(std::string& s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) { return !std::isspace(ch); }));
}

// trim from end (in place)
inline void rtrim(std::string& s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) { return !std::isspace(ch); }).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string& s) {
    ltrim(s);
    rtrim(s);
}

void populateLiveOilEntries(std::ifstream& in, std::vector<LiveOilTable::RsEntry>& entries) {
    auto line = std::string{};
    while (std::getline(in, line)) {
        trim(line);
        auto tokens = split(line, ' ');
        if (tokens.size() == 1 && tokens[0] == "/") return;
        if (tokens.size() == 4 && tokens[3] != "/") {
            // new entry
            auto entry = LiveOilTable::RsEntry{};
            entry.rs = stod(tokens[0]);
            entry.p.push_back(stod(tokens[1]));
            entry.fvf.push_back(stod(tokens[2]));
            entry.mu.push_back(stod(tokens[3]));
            entries.push_back(entry);
        } else {
            // otherwise keep growing the most recent entry
            entries.back().p.push_back(stod(tokens[0]));
            entries.back().fvf.push_back(stod(tokens[1]));
            entries.back().mu.push_back(stod(tokens[2]));
        }
    }
}

void populateDeadFluidEntries(std::ifstream& in, std::vector<DeadFluidTable::Entry>& entries) {
    auto line = std::string{};
    while (std::getline(in, line)) {
        trim(line);
        auto tokens = split(line, ' ');
        if (tokens.size() == 1 && tokens[0] == "/") return;
        entries.push_back({stod(tokens[0]), stod(tokens[1]), stod(tokens[2])});
    }
}

std::tuple<double, double, double> readDensities(std::ifstream& in) {
    auto line = std::string{};
    std::getline(in, line);
    trim(line);
    auto tokens = split(line, ' ');
    return std::make_tuple(stod(tokens[0]), stod(tokens[1]), stod(tokens[2]));
}
}  // namespace

PVTTable::PVTTable(const std::string& filename) {
    std::ifstream in(filename);
    if (!in.is_open()) {
        std::cout << "File could not be opened!\n";
        return;
    }
    auto line = std::string{};
    while (std::getline(in, line)) {
        if (line.find("DENSITY") != std::string::npos) {
            auto densities = readDensities(in);
            oilSurfaceDensity = std::get<0>(densities);
            gasSurfaceDensity = std::get<2>(densities);
        } else if (line.find("PVTO") != std::string::npos) {
            populateLiveOilEntries(in, liveOilTable.entries);
            std::cout << "# of Live Oil entries: " << liveOilTable.entries.size() << '\n';
        } else if (line.find("PVDG") != std::string::npos) {
            populateDeadFluidEntries(in, deadFluidTable.entries);
            std::cout << "# of Dead Fluid entries: " << deadFluidTable.entries.size() << '\n';
        }
    }
    std::cout << std::endl;
}
