#ifndef PVTEXTRAPOLATE_SCRIPTABLE_H
#define PVTEXTRAPOLATE_SCRIPTABLE_H

#include "ghc/filesystem.hpp"
#include <pybind11/embed.h>

#include <iostream>
#include <string>

namespace fs = ghc::filesystem;
namespace py = pybind11;

template <typename T>
class Scriptable {
  public:
    void runScript() {
        if (!validatePath()) {
            return;
        }
        try {
            auto pyModule = py::module::import(scriptPath.stem().string().c_str());
            auto entryPoint = pyModule.attr("run");
            T *derived = static_cast<T *>(this);
            entryPoint(derived);
        } catch (std::exception const& e) {
            std::cout << e.what() << std::endl;
        }
    }
    fs::path scriptPath;

  private:
    Scriptable() = default;
    friend T;

    bool validatePath() {
        bool result = false;
        if (!scriptPath.has_filename()) {
            std::cout << "Script path " << scriptPath.string() << " does not contain a filename.\n";
        } else if (scriptPath.extension() != ".py") {
            std::cout << "File " << scriptPath.string() << " should have a .py extension.\n";
        } else if (!fs::exists(scriptPath)) {
            std::cout << "File " << scriptPath.string() << " does not exist.\n";
        } else {
            result = true;
        }
        return result;
    }
};

#endif  //PVTEXTRAPOLATE_SCRIPTABLE_H
