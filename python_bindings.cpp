#include "PVTTable.h"

#include <pybind11/embed.h>
#include <pybind11/numpy.h>
#include <pybind11/stl_bind.h>

PYBIND11_MAKE_OPAQUE(std::vector<double>);
PYBIND11_MAKE_OPAQUE(std::vector<LiveOilTable::RsEntry>);

namespace py = pybind11;
using namespace py::literals;

namespace {
// Helper class that defines a mapping between std::vector and a numpy array
// on top of the existing pybind11 functionality
template <typename type_, typename... options>
class SRTPyClass : public py::class_<type_, options...> {
  public:
    using py::class_<type_, options...>::class_;
    using type = type_;
    using vectorD = std::vector<double>;

    template <typename C, typename... Extra>
    SRTPyClass& def_readvec_writenp(const char* name, vectorD C::*pm, const Extra&... extra) {
        static_assert(std::is_base_of<C, type>::value,
                      "def_readwrite() requires a class member (or base class member)");
        py::cpp_function fget([pm](const type& c) -> const vectorD& { return c.*pm; }, py::is_method(*this));
        py::cpp_function fset(
            [pm](type& c, py::array_t<double> array) {
                py::buffer_info info = array.request();
                if (info.ndim != 1) throw std::runtime_error("Array with ndim value of 1 is expected!");
                auto dataPtr = static_cast<double*>(info.ptr);
                (c.*pm).assign(dataPtr, dataPtr + info.size);
            },
            py::is_method(*this));

        py::class_<type_, options...>::def_property(name, fget, fset, py::return_value_policy::reference_internal,
                                                    extra...);
        return *this;
    };
};
}  // namespace

PYBIND11_EMBEDDED_MODULE(pvt, m) {
    py::bind_vector<std::vector<double>>(m, "VecDbl", py::module_local(false));
    py::bind_vector<std::vector<LiveOilTable::RsEntry>>(m, "LiveOilEntries", py::module_local(false));
    py::bind_vector<std::vector<DeadFluidTable::Entry>>(m, "DeadGasEntries", py::module_local(false));

    SRTPyClass<LiveOilTable::RsEntry>(m, "LiveOilEntry")
        .def_readvec_writenp("p", &LiveOilTable::RsEntry::p)
        .def_readvec_writenp("fvf", &LiveOilTable::RsEntry::fvf)
        .def_readvec_writenp("mu", &LiveOilTable::RsEntry::mu)
        .def_readwrite("rs", &LiveOilTable::RsEntry::rs)
        .def(py::init<>());

    py::class_<LiveOilTable>(m, "LiveOilTable")
        .def(py::init<>())
        .def_readwrite("entries", &LiveOilTable::entries);

    py::class_<DeadFluidTable::Entry>(m, "DeadFluidEntry")
        .def(py::init<>())
        .def_readwrite("p", &DeadFluidTable::Entry::p)
        .def_readwrite("fvf", &DeadFluidTable::Entry::fvf)
        .def_readwrite("mu", &DeadFluidTable::Entry::mu);

    py::class_<DeadFluidTable>(m, "DeadFluidTable")
        .def(py::init<>())
        .def_readwrite("entries", &DeadFluidTable::entries);

    py::class_<PVTTable>(m, "PVTTable")
        .def(py::init<>())
        .def(py::init<const std::string&>())
        .def_readwrite("oil_surface_density", &PVTTable::oilSurfaceDensity)
        .def_readwrite("gas_surface_density", &PVTTable::gasSurfaceDensity)
        .def_readwrite("live_oil_table", &PVTTable::liveOilTable)
        .def_readwrite("dead_fluid_table", &PVTTable::deadFluidTable);
}
