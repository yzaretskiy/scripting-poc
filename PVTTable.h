#ifndef PVTEXTRAPOLATE_PVTTABLE_H
#define PVTEXTRAPOLATE_PVTTABLE_H

#include "scriptable.h"

#include <memory>
#include <vector>

class LiveOilTable {
  public:
    LiveOilTable() = default;
    /** Holds fluid table values for a given value of Rs */
    struct RsEntry {
        /** Solution gas-oil ratio */
        double rs;
        /** Pressure */
        std::vector<double> p;
        /** Formation volume factor */
        std::vector<double> fvf;
        /** Viscosity */
        std::vector<double> mu;
    };
    /** Table entries grouped by Rs value */
    std::vector<RsEntry> entries;
};

class DeadFluidTable {
  public:
    DeadFluidTable() = default;
    /** Struct to hold fluid table values */
    struct Entry {
        /** Pressure */
        double p;
        /** Formation volume factor */
        double fvf;
        /** Viscosity */
        double mu;
    };

    /** Table entries */
    std::vector<Entry> entries;
};

class PVTTable : public Scriptable<PVTTable> {
  public:
    PVTTable() = default;
    explicit PVTTable(const std::string& filename);

    LiveOilTable liveOilTable;
    DeadFluidTable deadFluidTable;

    double oilSurfaceDensity;
    double gasSurfaceDensity;
};

#endif  // PVTTABLE_H
