import bisect

from matplotlib import pyplot as plt
import numpy as np
from scipy import interpolate
from scipy.optimize import minimize

# This import is required because 'run' below will be called from
# inside C++ and Python need to see the bindings at that point.
import pvt


def to_numpy(x):
    """Make sure we don't copy data during the C++ to numpy conversion"""
    return np.array(x, copy=False)


def oil_saturated_density(rho_o, rho_g, rs, fvf):
    return (rho_o + rho_g * rs) / fvf


def gas_density(rho_g, fvf):
    return rho_g / fvf


def plot_rs(axis, pvt_table,
            tick_label_size=15,
            grid_style=(0, (5, 10)),
            axis_label_size=18,
            sat_marker_size=10,
            unsat_marker_size=7):
    lo_table = pvt_table.live_oil_table.entries
    x = [e.p[0] for e in lo_table]
    y = [e.rs for e in lo_table]

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$R_s$', fontsize=axis_label_size)
    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)


def plot_fvf(axis, pvt_table,
             tick_label_size=15,
             grid_style=(0, (5, 10)),
             axis_label_size=18,
             sat_marker_size=10,
             unsat_marker_size=7):
    # oil fvf
    lo_table = pvt_table.live_oil_table.entries
    x = [e.p[0] for e in lo_table]
    y = [e.fvf[0] for e in lo_table]

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$B_o$', fontsize=axis_label_size, color='tab:blue')
    for entry in lo_table:
        axis.plot(entry.p, entry.fvf, '-^', color='tab:blue', markersize=unsat_marker_size)
    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)

    # gas fvf
    dg_table = pvt_table.dead_fluid_table.entries
    x = [e.p for e in dg_table]
    y = [e.fvf for e in dg_table]
    gas_ax = axis.twinx()
    gas_ax.plot(x, y, '-o', color='tab:red', markersize=sat_marker_size)
    gas_ax.set_ylabel('$B_g$', fontsize=axis_label_size, color='tab:red')
    gas_ax.tick_params(labelsize=tick_label_size)


def plot_viscosity(axis, pvt_table,
                   tick_label_size=15,
                   grid_style=(0, (5, 10)),
                   axis_label_size=18,
                   sat_marker_size=10,
                   unsat_marker_size=7):
    # oil viscosity
    lo_table = pvt_table.live_oil_table.entries
    x = [e.p[0] for e in lo_table]
    y = [e.mu[0] for e in lo_table]

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$\mu_o$', fontsize=axis_label_size, color='tab:blue')
    for entry in lo_table:
        axis.plot(entry.p, entry.mu, '-^', color='tab:blue', markersize=unsat_marker_size)
    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)

    # gas viscosity
    dg_table = pvt_table.dead_fluid_table.entries
    x = [e.p for e in dg_table]
    y = [e.mu for e in dg_table]

    gas_ax = axis.twinx()
    gas_ax.plot(x, y, '-o', color='tab:red', markersize=sat_marker_size)
    gas_ax.set_ylabel('$\mu_g$', fontsize=axis_label_size, color='tab:red')
    gas_ax.tick_params(labelsize=tick_label_size)


def extrapolate(x, y, end=None):
    xnew, ynew = [], []
    if end is not None and len(x) > 1 and end > x[-1]:
        f = interpolate.interp1d(x, y, kind='linear', fill_value='extrapolate')
        xnew = np.arange(x[-1], end, np.diff(x).mean())
        ynew = f(xnew)
    return xnew, ynew


def plot_inverse_fvf(axis, pvt_table,
                     extrapolate_to=None,
                     tick_label_size=15,
                     grid_style=(0, (5, 10)),
                     axis_label_size=18,
                     sat_marker_size=10,
                     unsat_marker_size=7):
    # oil fvf
    lo_table = pvt_table.live_oil_table.entries
    x = to_numpy([e.p[0] for e in lo_table])
    y = to_numpy([1 / e.fvf[0] for e in lo_table])
    xnew, ynew = extrapolate(x, y, extrapolate_to)

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.plot(xnew, ynew, '-o', color='tab:green', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$1 / B_o$', fontsize=axis_label_size, color='tab:blue')

    for entry in lo_table:
        x = to_numpy(entry.p)
        y = 1.0 / to_numpy(entry.fvf)
        xnew, ynew = extrapolate(x, y, extrapolate_to)
        axis.plot(x, y, '-^', color='tab:blue', markersize=unsat_marker_size)
        axis.plot(xnew, ynew, '-^', color='tab:green', markersize=sat_marker_size)

    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)
    axis.axhline(y=0.0, color='tab:blue', linestyle='-', linewidth=3.0)

    # gas fvf
    dg_table = pvt_table.dead_fluid_table.entries
    x = to_numpy([e.p for e in dg_table])
    y = to_numpy([1 / e.fvf for e in dg_table])
    xnew, ynew = extrapolate(x, y, extrapolate_to)
    gas_ax = axis.twinx()
    gas_ax.plot(x, y, '-o', color='tab:red', markersize=sat_marker_size)
    gas_ax.plot(xnew, ynew, '-o', color='tab:orange', markersize=sat_marker_size)
    gas_ax.set_ylabel('$1 / B_g$', fontsize=axis_label_size, color='tab:red')
    gas_ax.tick_params(labelsize=tick_label_size)
    gas_ax.axhline(y=0.0, color='tab:red', linestyle='-', linewidth=3.0)


def plot_inverse_fvt_mu(axis, pvt_table,
                        extrapolate_to=None,
                        tick_label_size=15,
                        grid_style=(0, (5, 10)),
                        axis_label_size=18,
                        sat_marker_size=10,
                        unsat_marker_size=7):
    # oil
    lo_table = pvt_table.live_oil_table.entries
    x = to_numpy([e.p[0] for e in lo_table])
    y = to_numpy([1 / e.fvf[0] / e.mu[0] for e in lo_table])
    xnew, ynew = extrapolate(x, y, extrapolate_to)

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.plot(xnew, ynew, '-o', color='tab:green', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$1 / B_o \mu_o$', fontsize=axis_label_size, color='tab:blue')

    for entry in lo_table:
        x = to_numpy(entry.p)
        y = 1.0 / (to_numpy(entry.fvf) * to_numpy(entry.mu))
        xnew, ynew = extrapolate(x, y, extrapolate_to)
        axis.plot(x, y, '-^', color='tab:blue', markersize=unsat_marker_size)
        axis.plot(xnew, ynew, '-o', color='tab:green', markersize=sat_marker_size)

    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)
    axis.axhline(y=0.0, color='tab:blue', linestyle='-', linewidth=3.0)

    # gas
    dg_table = pvt_table.dead_fluid_table.entries
    x = to_numpy([e.p for e in dg_table])
    y = to_numpy([1 / e.fvf / e.mu for e in dg_table])
    xnew, ynew = extrapolate(x, y, extrapolate_to)

    gas_ax = axis.twinx()
    gas_ax.plot(x, y, '-o', color='tab:red', markersize=sat_marker_size)
    gas_ax.plot(xnew, ynew, '-o', color='tab:orange', markersize=sat_marker_size)
    gas_ax.set_ylabel('$1 / B_g \mu_g$', fontsize=axis_label_size, color='tab:red')
    gas_ax.tick_params(labelsize=tick_label_size)
    gas_ax.axhline(y=0.0, color='tab:red', linestyle='-', linewidth=3.0)



def plot_density(axis, pvt_table,
                 tick_label_size=15,
                 grid_style=(0, (5, 10)),
                 axis_label_size=18,
                 sat_marker_size=10,
                 unsat_marker_size=7):
    rho_o = pvt_table.oil_surface_density
    rho_g = pvt_table.gas_surface_density

    # oil density
    lo_table = pvt_table.live_oil_table.entries
    x = [e.p[0] for e in lo_table]
    rs = to_numpy([e.rs for e in lo_table])
    fvf = to_numpy([e.fvf[0] for e in lo_table])
    y = oil_saturated_density(rho_o, rho_g, rs, fvf)

    axis.plot(x, y, '-o', color='tab:blue', markersize=sat_marker_size)
    axis.set_xlabel('$P$', fontsize=axis_label_size)
    axis.set_ylabel('$\\rho_o$', fontsize=axis_label_size, color='tab:blue')

    for entry in lo_table:
        x = to_numpy(entry.p)
        y = oil_saturated_density(rho_o, rho_g, entry.rs, to_numpy(entry.fvf))
        axis.plot(x, y, '-^', color='tab:blue', markersize=unsat_marker_size)

    axis.tick_params(labelbottom=True, labelsize=tick_label_size)
    axis.grid(linestyle=grid_style)

    # gas density
    gas_ax = axis.twinx()
    dg_table = pvt_table.dead_fluid_table.entries
    x = to_numpy([e.p for e in dg_table])
    fvf = to_numpy([e.fvf for e in dg_table])
    y = gas_density(rho_g, fvf)
    gas_ax.plot(x, y, '-o', color='tab:red', markersize=sat_marker_size)
    gas_ax.set_ylabel('$\\rho_g$', fontsize=axis_label_size, color='tab:red')
    gas_ax.tick_params(labelsize=tick_label_size)


def plot_pvt_curves(pvt_table, title, figsize=(20, 30), title_size=20,
                    top_adjust=0.95, extrapolate_to=None, **kwargs):
    fig, ax = plt.subplots(3, 2, sharex=True, figsize=figsize)
    fig.suptitle(title, fontsize=title_size)

    plot_rs(ax[0, 0], pvt_table, **kwargs)
    plot_fvf(ax[1, 0], pvt_table, **kwargs)
    plot_viscosity(ax[2, 0], pvt_table, **kwargs)
    plot_density(ax[0, 1], pvt_table, **kwargs)
    plot_inverse_fvf(ax[1, 1], pvt_table, extrapolate_to=extrapolate_to, **kwargs)
    plot_inverse_fvt_mu(ax[2, 1], pvt_table, extrapolate_to=extrapolate_to, **kwargs)

    fig.tight_layout()
    fig.subplots_adjust(top=top_adjust)

    plt.savefig(title+".png", dpi=150)


def density_cross(p_oil, p_gas, rs, fvf_o, fvf_g, rho_o, rho_g, eps=1e-6):
    """Find the pressure where gas density is just under the oil density.
    Returns corresponding p, rs, fvf_o and fvf_g values.
    """
    fvfg_inv = 1 / fvf_g
    
    # fit a line to the last two points of bo, rs and 1/bg.
    # bo = a1*p + b1, 1/bg = a2*p + b2, rs = a3*p + b3
    a1 = (fvf_o[-1] - fvf_o[-2]) / (p_oil[-1] - p_oil[-2])
    b1 = (fvf_o[-2] * p_oil[-1] - fvf_o[-1] * p_oil[-2]) / (p_oil[-1] - p_oil[-2])
    
    a2 = (fvfg_inv[-1] - fvfg_inv[-2]) / (p_gas[-1] - p_gas[-2])
    b2 = (fvfg_inv[-2] * p_gas[-1] - fvfg_inv[-1] * p_gas[-2]) / (p_gas[-1] - p_gas[-2])
    
    a3 = (rs[-1] - rs[-2]) / (p_oil[-1] - p_oil[-2])
    b3 = (rs[-2] * p_oil[-1] - rs[-1] * p_oil[-2]) / (p_oil[-1] - p_oil[-2])

    def fun(p):
        value = (rho_o + rho_g * (a3 * p + b3)) / (a1 * p + b1) - rho_g * (a2*p + b2) 
        return value * value

    p = minimize(fun, max(p_oil[-1], p_gas[-1]), tol=eps).x
    return p, a3 * p + b3, a1 * p + b1, 1 / (a2 * p + b2)


def viscosity_cross(p_oil, p_gas, visc_o, visc_g, eps=1e-6):
    """Find the pressure where gas viscosity is just under the oil viscosity.
    Returns corresponding p, mu values.
    """
    ln_visc_o = np.log(visc_o)
    ln_visc_g = np.log(visc_g)

    # fit a line to the last two points of ln(visc_o) and ln(visc_g).
    # ln(visc_o) = a1*p + b1, ln(visc_g) = a2*p + b2
    a1 = (ln_visc_o[-1] - ln_visc_o[-2]) / (p_oil[-1] - p_oil[-2])
    b1 = (ln_visc_o[-2] * p_oil[-1] - ln_visc_o[-1] * p_oil[-2]) / (p_oil[-1] - p_oil[-2])

    a2 = (ln_visc_g[-1] - ln_visc_g[-2]) / (p_gas[-1] - p_gas[-2])
    b2 = (ln_visc_g[-2] * p_gas[-1] - ln_visc_g[-1] * p_gas[-2]) / (p_gas[-1] - p_gas[-2])

    def fun(p):
        value = (a1 * p + b1) - (a2 * p + b2) 
        return value * value
    
    p = minimize(fun, max(p_oil[-1], p_gas[-1]), tol=eps).x
    return p, np.exp(a1 * p + b1)


def sorted_index(a, x):
    index = bisect.bisect_left(a, x)
    if index < len(a):
        if x == a[index]:
            return index
    return -1


def extrapolate_unsaturated_curve(unsat_entry, sat_entry):
    """Extrapolate an existing unsaturated curve to a new saturated point.
    Extrapolation procedure preserves compressibility and viscosibility.
    """
    new_p = sat_entry.p[0] + (to_numpy(unsat_entry.p) - unsat_entry.p[0])
    new_fvf = to_numpy(unsat_entry.fvf) * sat_entry.fvf[0] / unsat_entry.fvf[0]
    new_mu = to_numpy(unsat_entry.mu) * sat_entry.mu[0] / unsat_entry.mu[0]
    
    sat_entry.p = new_p
    sat_entry.fvf = new_fvf
    sat_entry.mu = new_mu


def extend_pvt_tables(pvt_table, add_surface_conditions=False, 
                      match_extrema=False, pressure_step=100):
    """Extend the oil and gas pvt curves to include provided
    pressure values. Ensure linear interpolation above these points
    reduces to constant interpolation instead.
 
    Parameters
    ----------
    add_surface_conditions : bool
        Include values at surface conditions.
    match_extema : bool
        Ensure that min and max values for oil and gas tables match.
    """
    new_table = pvt.PVTTable()
    new_table.oil_surface_density = pvt_table.oil_surface_density
    new_table.gas_surface_density = pvt_table.gas_surface_density
    
    lo_table = pvt_table.live_oil_table.entries
    dg_table = pvt_table.dead_fluid_table.entries
    rho_o = pvt_table.oil_surface_density
    rho_g = pvt_table.gas_surface_density

    p_oil = to_numpy([e.p[0] for e in lo_table])
    p_gas = to_numpy([e.p for e in dg_table])

    rs = to_numpy([e.rs for e in lo_table])
    fvf_o = to_numpy([e.fvf[0] for e in lo_table])
    fvf_g = to_numpy([e.fvf for e in dg_table])

    mu_oil = to_numpy([e.mu[0] for e in lo_table])
    mu_gas = to_numpy([e.mu for e in dg_table])

    rho_cross_pressure = density_cross(p_oil, p_gas, rs, fvf_o, fvf_g, rho_o, rho_g)
    visc_cross_pressure = viscosity_cross(p_oil, p_gas, mu_oil, mu_gas)

    # gas curves
    new_p_gas = np.array([rho_cross_pressure[0], rho_cross_pressure[0] + pressure_step, 
                          visc_cross_pressure[0], visc_cross_pressure[0] + pressure_step])
    
    extra_gas_values = []
    if match_extrema:
        if np.min(p_oil) < np.min(p_gas):
            extra_gas_values.append(np.min(p_oil))
        if np.max(p_oil) > np.max(p_gas):
            extra_gas_values.append(np.max(p_oil))
    
    new_p_gas = np.concatenate((new_p_gas.flat, extra_gas_values))

    f = interpolate.interp1d(p_gas, 1 / fvf_g, kind='linear', fill_value='extrapolate')
    new_fvf_g = 1 / f(new_p_gas)

    f = interpolate.interp1d(p_gas, np.log(mu_gas), kind='linear', fill_value='extrapolate')
    new_mu_gas = np.exp(f(new_p_gas))

    if add_surface_conditions:
        new_p_gas = np.concatenate((new_p_gas, [1.0]))
        new_fvf_g = np.concatenate((new_fvf_g, [1.0])) # not sure about this one
        new_mu_gas = np.concatenate((new_mu_gas, [np.exp(f(1.0))]))
    
    new_p_gas = np.concatenate((p_gas, new_p_gas))
    new_fvf_g = np.concatenate((fvf_g, new_fvf_g))
    new_mu_gas = np.concatenate((mu_gas, new_mu_gas))
    
    # sort by pressure
    indices = np.argsort(new_p_gas)
    new_p_gas = new_p_gas[indices]
    new_fvf_g = new_fvf_g[indices]
    new_mu_gas = new_mu_gas[indices]
        
    # ensure visc and density never cross
    new_mu_gas[new_p_gas > visc_cross_pressure[0]] = visc_cross_pressure[1]
    new_fvf_g[new_p_gas > rho_cross_pressure[0]] = rho_cross_pressure[3]
    
    for p, fvf, mu in zip(new_p_gas, new_fvf_g, new_mu_gas):
        new_entry = pvt.DeadFluidEntry()
        new_entry.p = p
        new_entry.fvf = fvf
        new_entry.mu = mu
        new_table.dead_fluid_table.entries.append(new_entry)    
    
    # saturated oil curves
    new_p_oil = np.array([rho_cross_pressure[0], rho_cross_pressure[0] + pressure_step, 
                          visc_cross_pressure[0], visc_cross_pressure[0] + pressure_step])
    extra_oil_values = []
    if match_extrema:
        if np.min(p_gas) < np.min(p_oil):
            extra_oil_values.append(np.min(p_gas))
        if np.max(p_gas) > np.max(p_oil):
            extra_oil_values.append(np.max(p_gas))
    new_p_oil = np.concatenate((new_p_oil.flat, extra_oil_values))
    
    f = interpolate.interp1d(p_oil, fvf_o, kind='linear', fill_value='extrapolate')
    new_fvf_o = f(new_p_oil)

    f = interpolate.interp1d(p_oil, rs, kind='linear', fill_value='extrapolate')
    new_rs = f(new_p_oil)

    f = interpolate.interp1d(p_oil, np.log(mu_oil), kind='linear', fill_value='extrapolate')
    new_mu_oil = np.exp(f(new_p_oil))

    if add_surface_conditions:
        new_p_oil = np.concatenate((new_p_oil, [1.0]))
        new_fvf_o = np.concatenate((new_fvf_o, [1.0]))
        new_rs = np.concatenate((new_rs, [0.0]))
        new_mu_oil = np.concatenate((new_mu_oil, [np.exp(f(1.0))]))

    new_p_oil = np.concatenate((p_oil, new_p_oil))
    new_rs = np.concatenate((rs, new_rs))
    new_fvf_o = np.concatenate((fvf_o, new_fvf_o))
    new_mu_oil = np.concatenate((mu_oil, new_mu_oil))

    # sort by pressure
    indices = np.argsort(new_p_oil)
    new_p_oil = new_p_oil[indices]
    new_rs = new_rs[indices]
    new_fvf_o = new_fvf_o[indices]
    new_mu_oil = new_mu_oil[indices]
        
    # ensure visc and density never cross
    new_mu_oil[new_p_oil > visc_cross_pressure[0]] = visc_cross_pressure[1]
    new_fvf_o[new_p_oil > rho_cross_pressure[0]] = rho_cross_pressure[2]
    new_rs[new_p_oil > rho_cross_pressure[0]] = rho_cross_pressure[1]
    
    rs_list = [e.rs for e in pvt_table.live_oil_table.entries]
    for p, rs, fvf, mu in zip(new_p_oil, new_rs, new_fvf_o, new_mu_oil):
        new_entry = pvt.LiveOilEntry()
        new_entry.rs = rs
        index = sorted_index(rs_list, rs)
        if index != -1:
            prev_entry = pvt_table.live_oil_table.entries[index]
            new_entry.p = prev_entry.p
            new_entry.fvf = prev_entry.fvf
            new_entry.mu = prev_entry.mu
        else:
            new_entry.p.append(p)
            new_entry.fvf.append(fvf)
            new_entry.mu.append(mu)

        new_table.live_oil_table.entries.append(new_entry)    

    # Extrapolate from nearest available unsaturated curve preserving compressibility
    entries = new_table.live_oil_table.entries
    last_entry = entries[len(entries) - 1]
    if len(last_entry.p) == 1:
        nearest = next(entry for entry in reversed(entries) if len(entry.p) > 1)
        extrapolate_unsaturated_curve(nearest, last_entry)
        
    for ridx, entry in enumerate(reversed(entries)):
        if len(entry.p) == 1:
            idx = len(entries)-ridx-1 # idx is forward index, ridx is reverse
            previous = entries[idx+1]
            extrapolate_unsaturated_curve(previous, entries[idx])
        
    return new_table


def run(table):
    pass
    """Entry point for the script."""
    #plot_pvt_curves(table, extrapolate_to=2000, title='PVT Tables Before')
    
    new_table = extend_pvt_tables(
        table, add_surface_conditions=False,
        match_extrema=True, pressure_step=10)
    table.dead_fluid_table = new_table.dead_fluid_table
    table.live_oil_table = new_table.live_oil_table

    #plot_pvt_curves(table, extrapolate_to=2000, title='PVT Tables After')
