import IPython
from traitlets.config import Config
c = Config()

c.InteractiveShellApp.exec_lines = [
    'import simulation',
]

# Start ipython with our configuration
IPython.start_ipython(config=c)
