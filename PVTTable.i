%module pvt_table
%feature("flatnested");

%{
  #define SWIG_FILE_WITH_INIT
  #include "PVTTable.h"
%}

%include "numpy.i"

%init %{
  import_array();
%}

%include "std_string.i"
%include "typemaps.i"
%include std_vector.i

%apply (double** ARGOUTVIEW_ARRAY1, int* DIM1) {(double** vec_out, int* n)}
%apply (double* IN_ARRAY1, int DIM1) {(double* vec_in, int n)}

%include "PVTTable.h"
%template(VectorD) std::vector<double>;
%template(LOEntries) std::vector<LiveOilTable::RsEntry>;
%template(DFEntries) std::vector<DeadFluidTable::Entry>;

%extend LiveOilTable::RsEntry{
  void p_fun(double** vec_out, int* n){
    *vec_out = &$self->p[0];
    *n = (int)$self->p.size();
  }

  void p_fun(double* vec_in, int n){
    $self->p.assign(vec_in, vec_in + n);
  }

  void fvf_fun(double** vec_out, int* n){
    *vec_out = &$self->fvf[0];
    *n = (int)$self->fvf.size();
  }

  void fvf_fun(double* vec_in, int n){
    $self->fvf.assign(vec_in, vec_in + n);
  }

  void mu_fun(double** vec_out, int* n){
    *vec_out = &$self->mu[0];
    *n = (int)$self->mu.size();
  }

  void mu_fun(double* vec_in, int n){
    $self->mu.assign(vec_in, vec_in + n);
  }
};
