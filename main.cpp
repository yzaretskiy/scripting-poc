#include "PVTTable.h"

#include "cxxopts.hpp"
#include <pybind11/embed.h>
#include <iostream>
#include <string>

namespace py = pybind11;
using namespace pybind11::literals;

struct State {
    PVTTable* table = nullptr;
};

// a global variable to export simulation results
auto state = State{};

PYBIND11_EMBEDDED_MODULE(simulation, m) {
    py::class_<State>(m, "State").def_readonly("pvt_table", &State::table);
    py::object py_state = py::cast(state);
    m.attr("state") = py_state;
}

int main(int argc, char *argv[]) {
    cxxopts::Options options("Scripting demo", "Demonstration of Python embedding");
    options.add_options()
        ("r, repl", "Launch REPL");
    auto result = options.parse(argc, argv);

    std::string filename{"PVT_EXTRAP_TEST.INC"};
    PVTTable table{filename};

    std::cout << "Live Oil table size before calling Python: "
              << table.liveOilTable.entries.size() << std::endl;

    py::scoped_interpreter guard{};
    // a pybind11 bug workaround
    py::module::import("sys").attr("argv").attr("append")("");

    table.scriptPath = "pvt_script.py";
    table.runScript();

    std::cout << "Live Oil table size after calling Python: "
              << table.liveOilTable.entries.size() << std::endl;

    if (result.count("r")) {
        state.table = &table;
        py::module::import("repl");
    }
    return 0;
}
